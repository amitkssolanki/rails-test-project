class DogBreedFetcher
  attr_reader :breed

  def initialize(name=nil)
    @name = name || "random"
    @breed = Breed.find_or_initialize_by(name: name)
  end

  def self.breeds
    fetch_all_breeds    
  end
  
  def fetch    
    return @breed if @breed.pic_url.present?

    @breed.pic_url = fetch_info
    @breed.save unless @breed.name == 'random'
    @breed
  end

  def self.fetch(name=nil)
    name ||= "random"
    DogBreedFetcher.new(name).fetch
  end

private
  def fetch_info
    begin
      if @name == 'random'
        JSON.parse(RestClient.get("https://dog.ceo/api/breeds/image/#{@name}").body)["message"]
      else
        JSON.parse(RestClient.get("https://dog.ceo/api/breed/#{@name}/images").body)["message"].first
      end
    rescue Object => e
      default_body
    end
  end

  def default_body
    {
      "status"  => "success",
      "message" => "https://images.dog.ceo/breeds/cattledog-australian/IMG_2432.jpg"
    }
  end
  
  def self.fetch_all_breeds
    begin
      # Ideally this should be cached to prevent misuse of api
      JSON.parse(RestClient.get("https://dog.ceo/api/breeds/list/all").body)["message"].keys
    rescue Object => e
      []
    end
  end
end
