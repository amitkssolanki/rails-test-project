class BreedsController < ApplicationController

  def index
    @breed = DogBreedFetcher.fetch(params[:name])
    @breeds = DogBreedFetcher.breeds
  end

end
